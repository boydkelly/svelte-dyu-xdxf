// Import Lunr.js
const lunr = require('lunr');

// Initialize Lunr.js
var idx = lunr(function () {
    this.field('headword');
    this.field('content');
});

// Assuming you have your HTML content in a variable called 'htmlContent'
// Parse the HTML to extract headwords and other content
var parser = new DOMParser();
var doc = parser.parseFromString(htmlContent, 'text/html');

// Iterate through the page content
doc.querySelectorAll('h4').forEach(function (h4) {
    idx.add({
        'headword': h4.textContent,
    });
});

// Iterate through all <div> elements with class 'ex_origin'
doc.querySelectorAll('div.ex_origin').forEach(function (div) {
    idx.add({
        'content': div.textContent
    });
});

// Other search-related functionality can also go here

// Export the Lunr.js index if needed
module.exports = idx;

