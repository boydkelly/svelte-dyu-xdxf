// svelte.config.js
import adapter from "@sveltejs/adapter-static";
import { optimizeImports } from "carbon-preprocess-svelte";

const config = {
  kit: {
    prerender: {
			handleHttpError: 'warn', 
      handleMissingId: 'warn'
    },
    adapter: adapter({
      // default options are shown. On some platforms
      // these options are set automatically — see below
      pages: "build",
      assets: "build",
      fallback: undefined, // may differ from host to host
      precompress: false,
      strict: false,
      prerender: {
        default: true,
      },
    }),
  },
  preprocess: [
    optimizeImports(),
  ],
};

export default config;
