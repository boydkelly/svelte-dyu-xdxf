import fs from "fs";
import Fuse from "fuse.js";

import jsonData from "./static/data.json" assert { type: "json" };
// Create a Fuse index from the JSON data.
const index = new Fuse(jsonData, {
  definition: ["h4Text"],
  examples: ["dtText"],
});

// Save the index to a file.
const indexFilePath = "./static/index.json";
const indexJson = JSON.stringify(index);
fs.writeFileSync(indexFilePath, indexJson);
