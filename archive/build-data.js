import asciidoctor from "@asciidoctor/core";
import fs from "fs";
import jsdom from "jsdom";
const { JSDOM } = jsdom;

const file = "./src/routes/dyu/+page.adoc";
console.log("Converting: ", file);
const processor = asciidoctor();
const html = processor.convertFile(file, {
  base_dir: "src/routes/dyu",
  to_file: false,
  standalone: false,
  safe: "unsafe",
  attributes: {
    "skip-front-matter": true,
    showtitle: true,
    includedir: "_include/",
    imagesdir: "/images",
    icons: "font",
    "allow-uri-read": "",
  },
});

const { window } = new JSDOM(html);
const doc = window.document;

const jsonData = [];

// Find all div.sect3 elements
const sect3Elements = doc.querySelectorAll("div.sect3");

sect3Elements.forEach((sect3Element) => {
  const h4Element = sect3Element.querySelector("h4");
  const h4Text = h4Element ? h4Element.textContent : null;
  const h4id = h4Element ? h4Element.id : null;

  const dtElements = sect3Element.querySelectorAll("dt.hdlist1");
  const dtText = Array.from(dtElements)
    .map((dtElement) => dtElement.textContent.trim())
    .filter((text) => text.trim() !== "");
  //   all examples but anywhere in the doc; needs href to preceeding h4

  if (dtText.length > 0) {
    jsonData.push({
      h4Text,
      dtText,
      h4ref: h4id, // Add the h4ref key with the id from h4Element
    });
  }
});

// Specify the output file path
const outputFilePath = "static/data.json";

// Write the jsonData to a JSON file
fs.writeFileSync(outputFilePath, JSON.stringify(jsonData, null, 2), "utf8");

console.log(`JSON data has been saved to ${outputFilePath}`);
