import fs from 'fs';
import cheerio from 'cheerio';
import Fuse from 'fuse.js';

// fuse apparently does this now
const removeAccents = (str) =>
  str.normalize('NFD').replace(/[\u0300-\u036F]/g, '');
const inputfilePath = './src/routes/dyu/Dioula.svelte';
const dataFilePath = './src/lib/data3.json';
const indexFilePath = './src/lib/index3.json';
const jsonData = [];
const exampleList = []; // Array to store all examples
const htmlContent = fs.readFileSync(inputfilePath, 'utf8');
const $ = cheerio.load(htmlContent);

const articles = $('article.dl');
articles.each((index, dlEl) => {
  const h4Id = $(dlEl).find('h4').attr('id').trim();
  const h4Text = $(dlEl).find('h4').text().trim();
  const h4NoAccents = removeAccents(h4Text);

  const examples = $(dlEl)
    .find('dl dt')
    .map((i, dtElement) => {
      const exampleText = $(dtElement).text().trim();
      exampleList.push({ h4Id, exampleText }); // Add example to the list
      console.log(h4Id, exampleText);
      return exampleText;
    })
    .get();

  jsonData.push({
    h4Id,
    h4NoAccents,
    h4Text,
    examples,
  });
});

fs.writeFileSync(dataFilePath, JSON.stringify(jsonData, null, 2), 'utf8');
console.log(`JSON data has been saved to ${dataFilePath}`);

// Create and save Fuse index with all examples
const options = { keys: ['h4Id', 'h4NoAccents', 'examples'] };
const index = Fuse.createIndex(options.keys, exampleList);
const indexJson = JSON.stringify(index);
fs.writeFileSync(indexFilePath, indexJson);
console.log(`JSON index has been saved to ${indexFilePath}`);
