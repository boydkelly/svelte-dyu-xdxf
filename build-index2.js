import fs from 'fs';
import * as cheerio from 'cheerio';
import Fuse from 'fuse.js';

const inputfilePath = './src/routes/dyu/Dioula.svelte';
const dataFilePath = './src/lib/data2.json';
const indexFilePath = './src/lib/index2.json';
const htmlContent = fs.readFileSync(inputfilePath, 'utf8');
const $ = cheerio.load(htmlContent);
const jsonData = [];
const removeAccents = (str) =>
  str.normalize('NFD').replace(/[\u0300-\u036F]/g, '');

const articles = $('article.dl');
articles.each((index, dlEl) => {
  const h4Id = $(dlEl).find('h4').attr('id').trim();
  const h4Text = $(dlEl).find('h4').text().trim();
  const spvText = $(dlEl).find('section.definition div.spv').text().trim();
  const h4NoDiacritics = removeAccents(h4Text);
  const examples = $(dlEl)
    .find('dl dt')
    .map((i, dtElement) => {
      return $(dtElement).text().trim();
    })
    .get();

  jsonData.push({
    h4Id,
    h4Text,
    h4NoDiacritics,
    spvText,
    examples,
  });
});

fs.writeFileSync(dataFilePath, JSON.stringify(jsonData, null, 2), 'utf8');
console.log(`JSON data has been saved to ${dataFilePath}`);

// Create and save Fuse index
//const options = {
//  keys: ['h4Text', 'h4NoDiacritics', 'spvText', 'examples'],
//};
//const index = Fuse.createIndex(options.keys, jsonData);
//const indexJson = JSON.stringify(index);
//fs.writeFileSync(indexFilePath, indexJson);
//console.log(`JSON index has been saved to ${indexFilePath}`);
