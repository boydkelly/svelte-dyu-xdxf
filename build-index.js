import fs from 'fs';
import jsdom from 'jsdom';
const { JSDOM } = jsdom;
import Fuse from 'fuse.js';

const inputfilePath = './src/routes/dyu/Dioula.svelte';
const dataFilePath = './src/lib/data.json';
const indexFilePath = './src/lib/index.json';
const htmlContent = fs.readFileSync(inputfilePath, 'utf8'); // Read the HTML content

const { window } = new JSDOM(htmlContent);
const doc = window.document;
const jsonData = [];
const removeAccents = (str) =>
  str.normalize('NFD').replace(/[\u0300-\u036F]/g, '');
const articles = doc.querySelectorAll('article.dl');

articles.forEach((article) => {
  const h4Element = article.querySelector('h4');
  const h4Text = h4Element ? h4Element.textContent : null;
  const h4Id = h4Element ? h4Element.id : null;
  const spvElement = article.querySelector('section.definition div.spv');
  const spvText = spvElement ? spvElement.textContent : null;
  const h4NoDiacritics = removeAccents(h4Text);

  if (h4Text.length > 0) {
    jsonData.push({
      h4Text,
      h4NoDiacritics,
      h4Id,
      spvText,
    });
  }
});

fs.writeFileSync(dataFilePath, JSON.stringify(jsonData, null, 2), 'utf8');
console.log(`JSON data has been saved to ${dataFilePath}`);

// Create a Fuse index from the JSON data.
// const options = { keys: ['h4Text', 'h4Id'] };
//const options = { keys: ['h4Text', 'h4Id', 'h4NoDiacritics', 'spvText'] };
//const index = Fuse.createIndex(options.keys, jsonData);
//const indexJson = JSON.stringify(index);
//fs.writeFileSync(indexFilePath, indexJson);
//console.log(`JSON index has been saved to ${indexFilePath}`);
